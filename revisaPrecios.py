import requests
from bs4 import BeautifulSoup
# from requests_html import AsyncHTMLSession
import array as arr

URL = []

# URL.append('https://www.cyberpuerta.mx/Computo-Hardware/Componentes/Fuentes-de-Poder-para-PC-s/Fuente-de-Poder-Corsair-CX450M-Semi-Modular-80-PLUS-Bronze-ATX-120mm-450W.html')
# URL[1] = 'https://es.aliexpress.com/item/1005004836144766.html'

listaArticulos = open("articulos.txt", "r")
articulos = listaArticulos.readlines()

# print(articulos)

for art in articulos:
    URL.append(art.strip())

# asession = AsyncHTMLSession()
# r = asession.get('https://www.aliexpress.com/item/32867673917.html?')

for url in URL:

    # h1 = r.html.find('h1')

    # print(h1[0].text)
    # price = r.html.find('.product-price-value')
    # print(price[0].text + "\n")

    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')

    name = soup.find("span", class_="name")
    price = soup.find('span', class_='price')
    stock = soup.find('span', class_='availability')
    print(name.text)
    print("$ " + price.text + " MXN")
    print(stock.text + "\n")

input("Análisis finalizado")
